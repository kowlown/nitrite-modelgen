<!-- BADGIE TIME -->
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/kowlown/nitrite-modelgen?branch=master)](https://gitlab.com/kowlown/nitrite-modelgen/-/commits/master)
[![Type of License](https://img.shields.io/gitlab/license/46670215 "License")](https://choosealicense.com/licenses/apache-2.0/)
[![Release badge](https://img.shields.io/gitlab/v/release/46670215 "Release")](https://gitlab.com/kowlown/nitrite-modelgen/-/releases)
[![Number of issues](https://img.shields.io/gitlab/issues/open/46670215 "Issues")](https://gitlab.com/kowlown/nitrite-modelgen/-/issues)
[![Last commit](https://img.shields.io/gitlab/last-commit/46670215 "Last commit")](https://gitlab.com/kowlown/nitrite-modelgen/-/commits/master)
<!-- END BADGIE TIME -->
# Nitrite Metamodel Generator

This is a metamodel generator which help to write filter for Nitrite DB.
There is an annotation to add to the object use for the collection in order to trigger the traversal of the attributes of the collection.

## Installation

Working to have a maven library

## Usage

```java
import com.mystnihon.nitrite.annotations.Collection;
import org.dizitart.no2.objects.Id;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.UUID;


@Collection
public class HistoryCollection implements Serializable {

    @Id
    private UUID documentId;
    private String label;
    private OffsetDateTime offsetDateTime;
	
	public HistoryCollection() {
		this(null);
	}

    public HistoryCollection(String label) {
        documentId = UUID.randomUUID();
        offsetDateTime = OffsetDateTime.now();
        this.label = label;
    }
}

```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)
