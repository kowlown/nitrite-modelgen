package com.mystnihon.nitrite.metadata;

/**
 * Represents an attribute of a Java type.
 * A persistent single-valued properties or fields.
 *
 * @param <Type> The type of the represented attribute
 *
 */
@SuppressWarnings({"unused", "java:S119"})
public class Attribute<Type> {
    private final String name;
    private final Class<Type> type;

    public Attribute(String name, Class<Type> type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Class<Type> getType() {
        return type;
    }
}
